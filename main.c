/* Name: main.c
 * Author: Degtyarev A. A.
 * Copyright: Degtyarev A. A. (c) 2017
 * License: BSD
 */
#include <avr/io.h>
//#include <util/delay.h>
#include <avr/interrupt.h>

#define F_CPU 1000000L //1Мгц

// вычисляем начальное значение TCNT0 для режима Normal
// - вычисляем период одного такта таймера Tt0 = k/Fcpu | Tt0 = 1024/1000000 = 0,001024 = 1.024 мс
// - вычисляем требуемое количество тактов для заданного интервала n = t/Tto | n=1мс/1.024мс = 1 = 1 тактов из 255.
// - вычисляем начальное значение для счетного регистра TCNT0 = 256 - n | TCNT0 = 256-1 = 255
// 255 начальное значение для интервала 1мс


#define T_POLL 255 // для таймера, начало отсчета до переполнения

#define INTERVAL_MS 5000 // интервал задержки выключения в мс

#define MCU_DAT 0 // вход тревоги с датчика
#define MCU_VCC_TEST 1 //проверка основного питания
#define MCU_RELAY 2 // выход на реле


#define H_PORT PORTB
#define H_DDR DDRB 
#define H_PIN PINB

typedef unsigned long dword; // беззнаковое 32х битное целое
extern volatile dword dmsec; // 
volatile dword dmsec;

unsigned char ActionAlarm = 0; // флаг активности MCU_DAT

ISR(TIMER0_OVF_vect){

	cli();

	dmsec++; // отметили интервал 1мс
   /*перезапись счетного регистра*/
   	TCNT0 = T_POLL;

 	sei();
}

int main(void)
{
//volatile unsigned char i;
	dmsec = 0; // инициализировали переменную

	H_DDR = 1<<MCU_RELAY|0<<MCU_DAT|0<<MCU_VCC_TEST; //настраиваем MCU_RELAY Выход, MCU_DAT - вход
	H_PORT = 1<<MCU_DAT|0<<MCU_VCC_TEST; // подтягиваем MCU_DAT к питанию | MCU_VCC_TEST к земле

	//T0
	TCCR0B = (1<<CS02)|(0<<CS01)|(1<<CS00); //xtall/1024
	TIFR = (1<<TOV0); // записываем 1 в TOV0
	TIMSK |= (1<<TOIE0); // разрешаем прерывание по событию переполнение

sei();// разрешаем прерывания


	// H_PORT &= ~(1<<MCU_RELAY); // ON
	 H_PORT |= 1<<MCU_RELAY; // OFF

while(1) {



		if (!(H_PIN & (1<<MCU_DAT)) & !(H_PIN & (1<<MCU_VCC_TEST)))
		{ // если MCU_DAT активен

			ActionAlarm = 1;
			dmsec = 0;
			H_PORT &= ~(1<<MCU_RELAY); // ON

		}

		if ((ActionAlarm ==1) & (dmsec>INTERVAL_MS)) // если флаг тревоки АКТИВЕН и время вышло, то выключаем питание.
		{
			H_PORT |= 1<<MCU_RELAY; // OFF
			ActionAlarm = 0;	
		}

} // while

    return 0;   /* never reached */

}//main
